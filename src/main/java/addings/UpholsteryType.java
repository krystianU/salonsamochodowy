package addings;

public enum UpholsteryType {

    VELOURS(0),
    LEATHER(1000),
    QUILTED_LEATHER(2000);

    private double price;

    UpholsteryType(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
