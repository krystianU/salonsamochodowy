package addings;

public enum FuelType {
    PETROL(0),
    DIESEL(10000),
    HYBRID(15000);

    private double price;

    FuelType(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
