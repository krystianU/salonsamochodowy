package addings;

public enum BodyType {
    SEDAN(0),
    PICK_UP(2000),
    HATCHBACK(1000),
    COMBI(1000);

    private double price;

    BodyType(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
