package addings;

public enum Color {
    RED(0),
    GREEN(0),
    BLUE(0),
    YELLOW(0),
    BLACK(0),
    WHITE(0),
    RED_METALLIC(1000),
    GREEN_METALLIC(1200),
    BLUE_METALLIC(1400),
    YELLOW_METALLIC(1600),
    BLACK_METALLIC(500),
    WHITE_METALLIC(500);
   private double price;

    Color(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
