package addings;

public enum Brand {
    BMW(80000),
    OPEL(65000),
    SKODA(60000),
    AUDI(70000),
    BENTLEY(200000),
    TOYOTA(70000),
    LEXUS(120000);
    private double price;

    Brand(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
