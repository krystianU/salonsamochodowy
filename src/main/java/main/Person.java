package main;

public class Person {

    //Amount of money
    private double amountOfMoney;

    public Person(double amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    public double getAmountOfMoney() {
        return amountOfMoney;
    }

    public void setAmountOfMoney(double amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
    }

    void printWalletBalance(){
        System.out.println("Your current wallet balance is: " + getAmountOfMoney() + " PLN");

    }

    public void subtractMoneyFromWallet(double amount){
        amountOfMoney -= amount;
    }

    public void addSpentMoney(double amount){
        double amountOfMoney = getAmountOfMoney();
        amountOfMoney+=amount;
        setAmountOfMoney(amountOfMoney);
    }
}
