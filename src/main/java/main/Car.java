package main;

import addings.*;

public class Car {
    private Brand brand;
    private Color color;
    private BodyType bodyType;
    private UpholsteryType upholsteryType;
    private FuelType fuelType;
    private double price;

    public Car(){

    }

    public Car(Brand brand, Color color, BodyType bodyType, UpholsteryType upholsteryType, FuelType fuelType, double price) {
        this.brand = brand;
        this.color = color;
        this.bodyType = bodyType;
        this.upholsteryType = upholsteryType;
        this.fuelType = fuelType;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand=" + brand +
                ", color=" + color +
                ", bodyType=" + bodyType +
                ", upholsteryType=" + upholsteryType +
                ", fuelType=" + fuelType +
                '}';
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public BodyType getBodyType() {
        return bodyType;
    }

    public void setBodyType(BodyType bodyType) {
        this.bodyType = bodyType;
    }

    public UpholsteryType getUpholsteryType() {
        return upholsteryType;
    }

    public void setUpholsteryType(UpholsteryType upholsteryType) {
        this.upholsteryType = upholsteryType;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}

