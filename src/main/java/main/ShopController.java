package main;

import addings.*;

import javax.swing.*;
import java.util.Scanner;


public class ShopController {
    Car car;
    Person person;
    private int errorCounter = 0;
    int wallet;


    public void printHelloAndProvideWalletBalance() {
        Scanner s = new Scanner(System.in);
        System.out.println("Hello, please provide your wallet balance in PLN: ");
        wallet = s.nextInt();
        person = new Person(wallet);
        person.printWalletBalance();
        car = new Car();
        checkIfEnoughMoney(person.getAmountOfMoney());
    }

    private void shopping() {
        chosenCar();
        errorCounter = 0;
        choseColor();
        errorCounter = 0;
        chooseBodyType();
        errorCounter = 0;
        chooseUpholsteryType();
        errorCounter = 0;
        chooseFuelType();
    }

    private void endProgram() {
        System.out.println("Too many wrong choices, good bye");
        throw new IllegalStateException("End of program");
    }

    private void walletChangeBrand(double amount) {
        double checkIfEnoughMoney = person.getAmountOfMoney() - amount;
        if (checkIfEnoughMoney >= 0) {
            person.setAmountOfMoney(checkIfEnoughMoney);
            person.printWalletBalance();
        } else {
            errorCounter++;
            if (errorCounter < 3) {
                System.out.println("You don't have enough money!");
                chosenCar();
            } else {
                endProgram();
            }
        }
    }

    private void chosenCar() {
        Scanner s = new Scanner(System.in);
        int chosenOption;
        int index = 1;
        for (Brand brand : Brand.values()) {
            System.out.println(index + ". " + brand + ", cost: " + brand.getPrice());
            index++;
        }
        System.out.println("Choose your car");
        chosenOption = s.nextInt();
        switch (chosenOption) {
            case 1:
                walletChangeBrand(Brand.BMW.getPrice());
                car.setBrand(Brand.BMW);
                return;
            case 2:
                walletChangeBrand(Brand.OPEL.getPrice());
                car.setBrand(Brand.OPEL);
                return;
            case 3:
                walletChangeBrand(Brand.SKODA.getPrice());
                car.setBrand(Brand.SKODA);
                return;
            case 4:
                walletChangeBrand(Brand.AUDI.getPrice());
                car.setBrand(Brand.AUDI);
                return;
            case 5:
                walletChangeBrand(Brand.BENTLEY.getPrice());
                car.setBrand(Brand.BENTLEY);
                return;
            case 6:
                walletChangeBrand(Brand.TOYOTA.getPrice());
                car.setBrand(Brand.TOYOTA);
                return;
            case 7:
                walletChangeBrand(Brand.LEXUS.getPrice());
                car.setBrand(Brand.LEXUS);
                return;
            default:
                System.out.println("Wrong value, try again");
                chosenCar();
        }
    }

    private void choseColor() {
        Scanner s = new Scanner(System.in);
        int chosenOption;
        int index = 1;
        System.out.println("Choose color of your car:");
        for (Color color : Color.values()) {
            System.out.println(index + ". " + color + ", cost: " + color.getPrice());
            index++;
        }

        chosenOption = s.nextInt();
        switch (chosenOption) {
            case 0:
                person.addSpentMoney(car.getBrand().getPrice());
                chosenCar();
                break;
            case 1:
                car.setColor(Color.RED);
                return;
            case 2:
                car.setColor(Color.GREEN);
                return;
            case 3:
                car.setColor(Color.BLUE);
                return;
            case 4:
                car.setColor(Color.YELLOW);
                return;
            case 5:
                car.setColor(Color.BLACK);
                return;
            case 6:
                car.setColor(Color.WHITE);
                return;
            case 7:
                walletChangeColor(Color.RED_METALLIC.getPrice());
                car.setColor(Color.RED_METALLIC);
                return;
            case 8:
                walletChangeColor(Color.GREEN_METALLIC.getPrice());
                car.setColor(Color.GREEN_METALLIC);
                return;
            case 9:
                walletChangeColor(Color.BLUE_METALLIC.getPrice());
                car.setColor(Color.BLUE_METALLIC);
                return;
            case 10:
                walletChangeColor(Color.YELLOW_METALLIC.getPrice());
                car.setColor(Color.YELLOW_METALLIC);
                return;
            case 11:
                walletChangeColor(Color.BLACK_METALLIC.getPrice());
                car.setColor(Color.BLACK_METALLIC);
                return;
            case 12:
                walletChangeColor(Color.WHITE_METALLIC.getPrice());
                car.setColor(Color.WHITE_METALLIC);
                return;
            default:
                System.out.println("Wrong value, try again");
                choseColor();
        }
    }

    private void walletChangeBodyType(double amount) {
        double checkIfEnoughMoney = person.getAmountOfMoney() - amount;
        if (checkIfEnoughMoney >= 0) {
            person.setAmountOfMoney(checkIfEnoughMoney);
            person.printWalletBalance();
        } else {
            errorCounter++;
            if (errorCounter < 3) {
                System.out.println("You don't have enough money!");
                chooseBodyType();
            } else {
                endProgram();
            }
        }
    }

    private void chooseBodyType() {
        Scanner s = new Scanner(System.in);
        System.out.println("Please choose body type of your car: ");
        int chosenOption;
        int index = 1;
        for (BodyType bodyType : BodyType.values()) {
            System.out.println(index + ". " + bodyType + ", cost: " + bodyType.getPrice());
            index++;
        }

        chosenOption = s.nextInt();
        switch (chosenOption) {
            case 1:
                car.setBodyType(BodyType.SEDAN);
                return;
            case 2:
                walletChangeBodyType(BodyType.PICK_UP.getPrice());
                car.setBodyType(BodyType.PICK_UP);
                return;
            case 3:
                walletChangeBodyType(BodyType.HATCHBACK.getPrice());
                car.setBodyType(BodyType.HATCHBACK);
                return;
            case 4:
                walletChangeBodyType(BodyType.COMBI.getPrice());
                car.setBodyType(BodyType.COMBI);
                return;
            default:
                System.out.println("Wrong value, try again");
                chooseBodyType();
        }
    }


    private void walletChangeColor(double amount) {
        double checkIfEnoughMoney = person.getAmountOfMoney() - amount;
        if (checkIfEnoughMoney >= 0) {
            person.setAmountOfMoney(checkIfEnoughMoney);
            person.printWalletBalance();
        } else {
            errorCounter++;
            if (errorCounter < 3) {
                System.out.println("You don't have enough money!");
                choseColor();
            } else {
                endProgram();
            }
        }
    }

    private void walletChangeUpholsteryType(double amount) {
        double checkIfEnoughMoney = person.getAmountOfMoney() - amount;
        if (checkIfEnoughMoney >= 0) {
            person.setAmountOfMoney(checkIfEnoughMoney);
            person.printWalletBalance();

        } else {
            errorCounter++;
            if (errorCounter < 3) {
                System.out.println("You don't have enough money!");
                chooseUpholsteryType();
            } else {
                endProgram();
            }
        }
    }

    private void chooseUpholsteryType() {

        Scanner s = new Scanner(System.in);
        int index = 1;
        int chosenOption;
        System.out.println("Choose upholstery type:");
        for (UpholsteryType upholsteryType : UpholsteryType.values()) {
            System.out.println(index + ". " + upholsteryType + ", cost: " + upholsteryType.getPrice());
            index++;
        }
        chosenOption = s.nextInt();

        switch (chosenOption) {
            case 1:
                car.setUpholsteryType(UpholsteryType.VELOURS);
                return;
            case 2:
                walletChangeUpholsteryType(UpholsteryType.LEATHER.getPrice());
                car.setUpholsteryType(UpholsteryType.LEATHER);
                return;
            case 3:
                walletChangeUpholsteryType(UpholsteryType.QUILTED_LEATHER.getPrice());
                car.setUpholsteryType(UpholsteryType.QUILTED_LEATHER);
                return;
            default:
                System.out.println("Wrong value, try again");
                chooseUpholsteryType();
        }
    }

    private void walletChangeFuelType(double amount) {
        double checkIfEnoughMoney = person.getAmountOfMoney() - amount;
        if (checkIfEnoughMoney >= 0) {
            person.setAmountOfMoney(checkIfEnoughMoney);
            person.printWalletBalance();

        } else {
            errorCounter++;
            if (errorCounter < 3) {
                System.out.println("You don't have enough money!");
                chooseFuelType();
            } else {
                endProgram();
            }
        }
    }

    private void chooseFuelType() {
        Scanner s = new Scanner(System.in);
        int chosenOption;
        int index = 1;
        for (FuelType fuelType : FuelType.values()) {
            System.out.println(index + ". " + fuelType + ", cost: " + fuelType.getPrice());
            index++;
        }
        System.out.println("Choose fuel type:");
        chosenOption = s.nextInt();

        switch (chosenOption) {
            case 1:
                car.setFuelType(FuelType.PETROL);
                printSummary();
                break;
            case 2:
                walletChangeFuelType(FuelType.DIESEL.getPrice());
                car.setFuelType(FuelType.DIESEL);
                printSummary();
                break;
            case 3:
                walletChangeFuelType(FuelType.HYBRID.getPrice());
                car.setFuelType(FuelType.HYBRID);
                printSummary();
                break;
            default:
                System.out.println("Wrong value, try again");
                chooseFuelType();
        }
    }

    private void printSummary() {
        System.out.println(car.toString());
        System.out.println("Total car cost: " + (person.getAmountOfMoney() - wallet) * -1 + " PLN");
    }

    private void checkIfEnoughMoney(double price) {
        if (price >= 60000) {
            shopping();
        } else {
            System.out.println("You don't have enough money to continue shopping!");
        }
    }

}

